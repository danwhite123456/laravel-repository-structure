<?php namespace App\Repositories\Contracts;

interface BaseInterface {

	public function get_single($id);

	public function get_single_no_relationships($id);

	public function get_all($args, $relationships);

	public function get_multiple_by_id($ids);

	public function create($args);

	public function delete($id);

	public function update($id,$args);

}
