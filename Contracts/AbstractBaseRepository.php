<?php namespace App\Repositories\Contracts;

abstract class AbstractBaseRepository implements BaseInterface{


	protected $protected = [];


	public function get_single($id){
		$query = $this->model->where('id','=',$id)->firstOrFail();
		return $query;
	}



	public function get_all($args = false, $relationships = false, $order = false, $order_asc = 'ASC'){
		if($args){
			foreach ($args as $field => $arg) {
				if (empty($field) || empty($arg)) {
					continue;
				}
				if(strstr($field,' >') || strstr($field,' <') || strstr($field,' !=') || strstr($field,' =')){
					$field = explode(' ',$field);
					$operator = $field[1];
					$field = $field[0];
				} else	{
					$operator = '=';
				}
				empty($query) ? $query = $this->model->where($field,$operator, $arg) : $query->where($field,$operator, $arg);
			}
		}
		if ($relationships) {
			empty($query) ? $query = $this->model->with($relationships) : $query->with($relationships);
		}
		if($order){
			empty($query) ?$this->model->orderBy($order) :$query->orderBy($order, $order_asc);
		}
		if(isset($query)){
			return $query->get();
		} else {
			return $this->model->get();
		}
	}


	public function get_multiple_by_id($ids){
		return $this->model-> whereIn('id',$ids)->get();
	}



	public function create($args){
		if(count($args) > 1){
		return $this->model->insert($args);
		}
		return $this->model->insertGetId($args[0]);
	}




	public function delete($id)	{
		if (in_array($id, $this->protected)) return false;

		$instance = $this->model->where('id', $id)->firstOrFail();

		if ($instance->delete()) {
			return true;
		}
		return false;
	}




	public function update($id, $args){
		//dd($id);
		$instance = $this->model->findOrFail($id);
		foreach ($args as $field => $arg) {
			$instance->{$field} = $arg;
		}
		$instance->save();
		return $instance;
	}
	
}
