<?php

namespace App\Repositories\Users;

use App\User;
use App\Repositories\Contracts\AbstractBaseRepository;
use App\Repositories\Contracts\UserRepositoryInterface;

class UserRepository extends AbstractBaseRepository implements UserRepositoryInterface{
	protected $model;
	protected $relationships;
	protected $order;

	public function __construct()
	{
		$this->model = new User;
	}
	
	public function delete($id)	{
		if (in_array($id, $this->protected)) return false;

		$instance = $this->model->where('id', $id)->firstOrFail();
		
		if ($instance->update(['status'=>2])) {
			return true;
		}
		return false;
	}
}