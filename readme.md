### Simple Laravel Repository Structure

A simple recyclable Laravel repository structure

## Installation

Simply paste into your /App dir or wherever you want but then update the namespaces

## Usage

I'm a fan of dependency injection but you can do either...


    use App\Repositories\Users as User;

    class UserController extends Controller
    {
        protected $user;

        public function __construct(User $user)
        {
            $this->user = $user;
        }

        public function account()
        {
            return $this->user->get_single($user_id);
        }
    }


... or


    class UserController extends Controller
    {
        public function account()
        {
            return \App\Repositories\Users::get_single($user_id);
        }
    }


## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

0.1 - Contracts and example User Repository

## Credits

Daniel White (hi@dwhite.me)

## License

Share however you want.
